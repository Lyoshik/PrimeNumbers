package com.example;

import java.io.FileNotFoundException;
import java.util.List;

public interface Tools{
    int transform(String string);
    List<Integer> find(String first, String last);
    void print();
}
