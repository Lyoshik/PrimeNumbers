package com.example;

import java.util.ArrayList;
import java.util.List;

public class PrimeNumbers implements Tools{
    public boolean isSimple = false;
    List<Integer> simpleNums = new ArrayList<>();

    @Override
    public int transform(String string) throws NumberFormatException{
        int number = Integer.parseInt(string);
        return number;
    }

    @Override
    public List<Integer> find(String first, String last) throws WrongIntervalException {
        if (transform(first)>=transform(last)){
            throw new WrongIntervalException();
        }
        if (transform(first)<=0 || transform(last)<=0){
            throw new NumberFormatException();
        }
        for (int i = transform(first); i <= transform(last); i++) {
            for(int j = 2; j < i; j++) {
                if(i%j != 0){
                    isSimple = true;
                }
                else {
                    isSimple = false;
                    break;
                }
            }
            if(isSimple) simpleNums.add(i);
        }
        return simpleNums;
    }

    @Override
    public void print() {
        simpleNums.forEach(element-> System.out.println(element+ " is a simple number"));
    }
}
