package com.example;

import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class PrimeNumbersTest {
    PrimeNumbers primeNumbers = new PrimeNumbers();

    @Test(expected = NumberFormatException.class)
    public void testTransform() {
        primeNumbers.transform("qwe");
    }

    @Test
    public void testFind(){
        primeNumbers.find("2", "33");
        assertTrue(primeNumbers.simpleNums.get(3).toString().equals("11"));
        assertFalse(primeNumbers.simpleNums.contains("10"));
    }

    @Test (expected = WrongIntervalException.class)
    public void testInterval() {
        primeNumbers.find("66", "33");
    }

    @Test (expected = NumberFormatException.class)
    public void testForNegativeNumbers(){
        primeNumbers.find("-35", "8");
    }
}
